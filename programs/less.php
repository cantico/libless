<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';



/**
 * This functionality provides the possibility to personnalize css files.
 */
class Func_Less extends bab_Functionality
{

	/**
	 * @var string
	 */
	protected $compiledCssPath = null;

	protected $compiledCssBaseUrl = null;



	/**
	 *
	 * @param string $path
	 */
	public function setCompiledCssPath($path)
	{
		$this->compiledCssPath = $path;
		return $this;
	}





	/**
	*
	* @return string
	*/
	public function getDefaultCompiledCssPath()
	{
		$addon = bab_getAddonInfosInstance('LibLess');
		$ovidentiapath = realpath('.');
		$compiledCssPath = $ovidentiapath . '/' . $addon->getStylePath();
		return $compiledCssPath;
	}


	/**
	 *
	 * @return string
	 */
	public function getCompiledCssPath()
	{
		if (!isset($this->compiledCssPath)) {
			$this->compiledCssPath = $this->getDefaultCompiledCssPath();
		}
		return $this->compiledCssPath;
	}



	/**
	*
	* @param string $path
	*/
	public function setCompiledCssBaseUrl($url)
	{
		$this->compiledCssBaseUrl = $url;
		return $this;
	}




	/**
	*
	* @return string
	*/
	public function getCompiledCssBaseUrl()
	{
		if (!isset($this->compiledCssBaseUrl)) {
			$addon = bab_getAddonInfosInstance('LibLess');
			$this->compiledCssBaseUrl = $addon->getStylePath(); //$addon->getRelativePath();
		}
		return $this->compiledCssBaseUrl;
	}



	/**
	 *
	 *
	 * @param string $lessFilename
	 * @param array  $variables
	 */
	function getCssUrl($lessFilename, $variables = null, $force = false)
	{
		$cssFilename = $this->autoCompileLess($lessFilename, $variables, $force);

		return $this->getCompiledCssBaseUrl() . $cssFilename;
	}




	protected function autoCompileLess($inputFile, $variables = null, $force = false)
	{
		$destPath = $this->getCompiledCssPath();

		$input_uid = ','.(string) abs(crc32($inputFile));

		// load the cache
		$cacheFile = $destPath.$input_uid.'-'.basename($inputFile).".cache";

		if (file_exists($cacheFile)) {
			$cache = unserialize(file_get_contents($cacheFile));
		} else {
			$cache = $inputFile;
		}

		require_once dirname(__FILE__) . '/vendor/oyejorge/less.php/lessc.inc.php';

		$less = new lessc;

		if (isset($variables)) {
			$less->setVariables($variables);
		}

		$newCache = $less->cachedCompile($cache, $force);



		$destFilename = $input_uid.'-'.str_replace('.less', '.css', basename($inputFile));
		$outputFile = $destPath . $destFilename;

		if (!is_array($cache) || $newCache['updated'] > $cache['updated']) {

			$outputDir = new bab_Path(dirname($outputFile));
			if (!$outputDir->isDir()) {
				$outputDir->createDir();
			}
			file_put_contents($cacheFile, serialize($newCache));
			file_put_contents($outputFile, $newCache['compiled']);
		}

		return $destFilename;
	}




	public function removeCompiledFiles()
	{
		$defaultCompiledCssPath = $this->getDefaultCompiledCssPath();
		$defaultCompiledCssPath = new bab_Path($defaultCompiledCssPath);

		foreach ($defaultCompiledCssPath as $file) {
			/* @var $file bab_Path */
			if (!$file->isDir()) {
				$file->delete();
			}
		}
	}

}
