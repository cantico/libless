<?php


function LibLess_upgrade($version_base, $version_ini)
{
	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	$functionalities = new bab_functionalities();

	$addon = bab_getAddonInfosInstance('LibLess');

	if (false === $functionalities->register('less', $addon->getPhpPath() . 'less.php')) {
		return false;
	}

	return true;
}



function LibLess_onDeleteAddon() {

	require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
	$functionalities = new bab_functionalities();

	if (false === $functionalities->unregister('less')) {
		return false;
	}

	return true;
}



function LibLess_onPackageAddon()
{
	/* @var $Less Func_Less */
	$Less = bab_functionality::get('less');

	if (!$Less) {
		return false;
	}

	$defaultCompiledCssPath = $Less->getDefaultCompiledCssPath();

	$defaultCompiledCssPath = new bab_Path($defaultCompiledCssPath);
	foreach ($defaultCompiledCssPath as $file) {
		/* @var $file bab_Path */
		if (!$file->isDir()) {
			try {
				$file->delete();
			} catch(bab_FileAccessRightsException $e)
			{
				bab_debug($e->getmessage());
			}
		}
	}

	return true;
}
