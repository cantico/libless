;<?php/*

[general]
name                        ="LibLess"
version                     ="1.7.0.11"
description                 ="LessPhp package for Ovidentia"
description.fr              ="Librairie partagée fournissant les fonctionalités de LESS"
long_description.fr         ="README.md"
encoding                    ="UTF-8"
delete                      =1
ov_version                  ="7.8.0"
php_version                 ="5.1"
mysql_character_set_database="latin1,utf8"
addon_access_control        ="0"
author                      ="Laurent Choulette (laurent.choulette@cantico.fr)"
icon                        ="less-css-logo-48x48.png"
tags                        ="library,css,style"
;*/?>
